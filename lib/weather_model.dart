import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_smart_dialog/flutter_smart_dialog.dart';
import 'package:my_app/static_function.dart';

class WeatherModel extends ChangeNotifier{
  Map<String, dynamic> records;

  WeatherModel({
    required this.records,
  });

  void setRecords(Map<String, dynamic> json){
    records = json['records'];
    notifyListeners();
  }

  dynamic getWeatherElement(){
    dynamic location = records['location'];
    return location[0]['weatherElement'];
  }

  dynamic getWxArray(){
    dynamic we = getWeatherElement();
    dynamic wxArray = we[0]['time'];
    return wxArray;
  }

  dynamic getPoPArray(){
    dynamic we = getWeatherElement();
    dynamic popArray = we[1]['time'];
    return popArray;
  }

  dynamic getMinTempArray(){
    dynamic we = getWeatherElement();
    dynamic tempArray = we[2]['time'];
    return tempArray;
  }

  dynamic getMaxTempArray(){
    dynamic we = getWeatherElement();
    dynamic tempArray = we[4]['time'];
    return tempArray;
  }

  dynamic getCiArray(){
    dynamic we = getWeatherElement();
    dynamic ciArray = we[3]['time'];
    return ciArray;
  }


}
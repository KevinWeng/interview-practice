import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'Res/styles.dart';
import 'i18n/strings.g.dart';

class StaticFunction {
  static showWarningDialogWithThen(String title, String contentMsg, BuildContext context,
      {VoidCallback? onFinish, bool cancelable = false, TextAlign contentAlignment = TextAlign.center}) {
    showDialog(
        barrierDismissible: cancelable,
        context: context,
        builder: (context) {
          return StaticFunction._WarningDialog(title, contentMsg, context, contentAlignment);
        }).then((value) {
      if (onFinish != null) onFinish();
    });
  }

  static showWarningDialog(String title, String contentMsg, BuildContext context,
      [bool cancelable = false, TextAlign contentAlignment = TextAlign.center]) {
    showDialog(
        barrierDismissible: cancelable,
        context: context,
        builder: (context) {
          return StaticFunction._WarningDialog(title, contentMsg, context, contentAlignment);
        });
  }

  static AlertDialog _WarningDialog(String title, String contentMsg, BuildContext context, TextAlign contentAlignment) {
    return AlertDialog(
      insetPadding: EdgeInsets.all(ScreenUtil().setWidth(60)),
      title: Text(title, textAlign: TextAlign.center),
      titleTextStyle: Styles.textLargeBold.copyWith(color: Colors.red),
      content: Container(
        width: double.maxFinite,
        padding: EdgeInsets.symmetric(vertical: ScreenUtil().setHeight(50)),
        child: Text(contentMsg, textAlign: contentAlignment, style: Styles.textMidBold),
      ),
      actions: <Widget>[
        Center(
          child: SizedBox(
            width: ScreenUtil().setWidth(350),
            height: ScreenUtil().setWidth(80),
            child: ElevatedButton(
              onPressed: () => Navigator.pop(context, t.OK),
              child: Text(
                t.OK,
                style: Styles.textMidBold,
              ),
            ),
          ),
        ),
      ],
    );
  }

  static showOneButtonNoticeDialog(BuildContext context, String title, String contentMsg, VoidCallback positiveAction, String positiveString,
      [bool cancelable = false]) {
    showDialog(
        barrierDismissible: cancelable,
        context: context,
        builder: (context) {
          return StaticFunction._noticeDialog(context, title, contentMsg, positiveAction, () {}, positiveString, "", true, false);
        });
  }

  static showTwoButtonNoticeDialog(BuildContext context, String title, String contentMsg, VoidCallback positiveAction, VoidCallback negativeAction,
      String positiveString, String negativeString,
      [bool cancelable = false]) {
    showDialog(
        barrierDismissible: cancelable,
        context: context,
        builder: (context) {
          return StaticFunction._noticeDialog(context, title, contentMsg, positiveAction, negativeAction, positiveString, negativeString, true, true);
        });
  }

  static AlertDialog _noticeDialog(BuildContext context, String title, String contentMsg, VoidCallback positiveAction, VoidCallback negativeAction,
      String positiveString, String negativeString, bool isPositiveVisible, bool isNegativeVisible) {
    return AlertDialog(
      insetPadding: EdgeInsets.all(ScreenUtil().setWidth(60)),
      title: Text(title, textAlign: TextAlign.center),
      titleTextStyle: Styles.textLargeBold.copyWith(color: Colors.black),
      content: Container(
        width: double.maxFinite,
        padding: EdgeInsets.symmetric(vertical: ScreenUtil().setHeight(50)),
        child: Text(contentMsg, textAlign: TextAlign.center, style: Styles.textMidBold),
      ),
      actions: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Visibility(
              visible: isPositiveVisible,
              child: Expanded(
                flex: 5,
                child: ElevatedButton(
                  onPressed: positiveAction,
                  child: Text(positiveString, style: Styles.textMid),
                ),
              ),
            ),
            Visibility(
              visible: isPositiveVisible && isNegativeVisible,
              child: const Spacer(
                flex: 1,
              ),
            ),
            Visibility(
              visible: isNegativeVisible,
              child: Expanded(
                flex: 5,
                child: ElevatedButton(
                  onPressed: negativeAction,
                  child: Text(
                    negativeString,
                    style: Styles.textMid,
                  ),
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }

}

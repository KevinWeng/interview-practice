import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_smart_dialog/flutter_smart_dialog.dart';
import 'package:dio/dio.dart';
import 'package:intl/intl.dart';
import 'package:my_app/weather_model.dart';
import 'package:provider/provider.dart';

import 'Res/colors.dart';
import 'Res/styles.dart';
import 'static_function.dart';

class WeatherPage extends StatefulWidget {
  const WeatherPage({Key? key}) : super(key: key);

  @override
  State<WeatherPage> createState() => _WeatherPageState();
}

class _WeatherPageState extends State<WeatherPage> {
  final dio = Dio();

  EdgeInsetsGeometry basicEdge = EdgeInsets.only(left: ScreenUtil().setWidth(30), right: ScreenUtil().setWidth(30), top: ScreenUtil().setHeight(30));
  final regionController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        centerTitle: true,
        title: const Text('Weather App'),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: basicEdge,
            child: Row(
              children: [
                Expanded(
                  child: TextField(
                    decoration: const InputDecoration(border: OutlineInputBorder(), hintText: '請輸入城市名稱'),
                    controller: regionController,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(20)),
                  child: SizedBox(
                    height: ScreenUtil().setHeight(130),
                    // this is the height of TextField
                    child: ElevatedButton(
                      onPressed: () async {
                        FocusManager.instance.primaryFocus?.unfocus();
                        if(await checkWifiState()){
                          await getWeatherAPI(regionController.text);
                        }
                      },
                      child: Text('確認', style: Styles.textMid),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: basicEdge,
            child: Text('一般天氣預報-今明36小時天氣預報', style: Styles.textSmallBold),
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(left: ScreenUtil().setWidth(30), right: ScreenUtil().setWidth(30), bottom: ScreenUtil().setHeight(30)),
              child: Container(
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(
                    Radius.circular(10),
                  ),
                  color: AppColors.darkGrey,
                ),
                child: Consumer<WeatherModel>(
                  builder: (context, weatherModel, child) {
                    return weatherModel.records.isNotEmpty
                        ? _buildResultSection(weatherModel)
                        : Text(
                            '輸入城市名稱',
                            style: Styles.textMid,
                          );
                  },
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildResultSection(WeatherModel weatherModel) {
    return Column(
      children: [
        _buildPeriodWeatherWidget(weatherModel, 0),
        _buildPeriodWeatherWidget(weatherModel, 1),
        _buildPeriodWeatherWidget(weatherModel, 2),
      ],
    );
  }

  Widget _buildPeriodWeatherWidget(WeatherModel weatherModel, int num) {
    //region Wx
    var wxArray = weatherModel.getWxArray();
    final startDate = DateTime.parse(wxArray[num]['startTime']);
    String formattedStartTime = DateFormat('MM/dd HH:mm').format(startDate);
    print(startDate);
    final endDate = DateTime.parse(wxArray[num]['endTime']);
    String formattedEndTime = DateFormat('MM/dd HH:mm').format(endDate);
    print(endDate);
    final wxParameterName = wxArray[num]['parameter']['parameterName'];

    //endregion

    //region PoP 降雨機率
    var popArray = weatherModel.getPoPArray();
    final popParameterName = popArray[num]['parameter']['parameterName'];
    //endregion

    //region Temperature 溫度
    var minTempArray = weatherModel.getMinTempArray();
    final minTempParameterName = minTempArray[num]['parameter']['parameterName'];
    final minTempParameterUnit = minTempArray[num]['parameter']['parameterUnit'];
    var maxTempArray = weatherModel.getMaxTempArray();
    final maxTempParameterName = maxTempArray[num]['parameter']['parameterName'];
    final maxTempParameterUnit = maxTempArray[num]['parameter']['parameterUnit'];
    //endregion

    //region CI 舒適度
    var ciArray = weatherModel.getCiArray();
    final ciParameterName = ciArray[num]['parameter']['parameterName'];
    //endregion

    return Padding(
      padding: basicEdge,
      child: Container(
        margin: EdgeInsets.zero,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
          color: AppColors.lightGrey,
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: ScreenUtil().setWidth(30)),
          child: Column(
            children: [
              _buildRowWidget('時段: ', '$formattedStartTime ~ $formattedEndTime'),
              _buildRowWidget('天氣現象: ', wxParameterName),
              _buildRowWidget('降雨機率: ', popParameterName + '%'),
              _buildRowWidget('溫度: ', minTempParameterName + '°' + minTempParameterUnit + ' ~ ' + maxTempParameterName + '°' + maxTempParameterUnit),
              _buildRowWidget('舒適度: ', ciParameterName, needLine: false),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildRowWidget(String title, String value, {bool needLine = true}) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(title, style: Styles.textSmall),
            Text(value, style: Styles.textSmall),
          ],
        ),
        Padding(
          padding: EdgeInsets.only(top: ScreenUtil().setHeight(2)),
          child: Visibility(
            visible: needLine,
            child: Container(
              height: ScreenUtil().setHeight(1),
              decoration: const BoxDecoration(
                shape: BoxShape.rectangle,
                color: Colors.black,
              ),
            ),
          ),
        ),
      ],
    );
  }

  Future<void> getWeatherAPI(String location) async {
    if (location.isEmpty) {
      StaticFunction.showWarningDialog("NOTICE", "請輸入城市名稱", context);
      return;
    }
    String token = "CWB-30BDC8DE-99E3-4BBA-8BB8-D1299AA5D911";

    dio.options.baseUrl = 'https://opendata.cwb.gov.tw';
    SmartDialog.showLoading();
    try {
      // 404
      final response = await dio.get(
        '/api/v1/rest/datastore/F-C0032-001',
        queryParameters: {'Authorization': token, 'locationName': location},
      );
      print(response);
      if (response.data['records']['location'].isEmpty) {
        StaticFunction.showWarningDialog("WARNING", "輸入的城市名稱無效", context);
      } else {
        WeatherModel weatherModel = Provider.of<WeatherModel>(context, listen: false);
        weatherModel.setRecords(response.data);
      }
    } on DioException catch (e) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx and is also not 304.
      if (e.response != null) {
        StaticFunction.showWarningDialog("ERROR", "認證或服務器存在未知問題", context);
      } else {
        // Something happened in setting up or sending the request that triggered an Error
        StaticFunction.showWarningDialog("ERROR", "抱歉，因未知原因失敗", context);
      }
    }

    SmartDialog.dismiss();
  }

  Future<bool> checkWifiState() async {
    try {
      final result = await InternetAddress.lookup('example.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        print('connected');
        return true;
      } else {
        StaticFunction.showWarningDialog("WARNING", "請確認網路狀態", context);
        return false;
      }
    } on SocketException catch (_) {
      print('not connected');
      StaticFunction.showWarningDialog("WARNING", "請確認網路狀態", context);
      return false;
    }
  }
}

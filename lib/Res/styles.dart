import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class Styles {
  static TextStyle textLargeBold = TextStyle(fontSize: ScreenUtil().setSp(80), fontWeight: FontWeight.bold);
  static TextStyle textLarge = TextStyle(fontSize: ScreenUtil().setSp(80));
  static TextStyle textMidBold = TextStyle(fontSize: ScreenUtil().setSp(60), fontWeight: FontWeight.bold);
  static TextStyle textMid = TextStyle(fontSize: ScreenUtil().setSp(60));
  static TextStyle textSmallBold = TextStyle(fontSize: ScreenUtil().setSp(40), fontWeight: FontWeight.bold);
  static TextStyle textSmall = TextStyle(fontSize: ScreenUtil().setSp(40));
  static TextStyle itemFontSize = TextStyle(fontSize: ScreenUtil().setSp(50), color: Colors.black, fontWeight: FontWeight.normal);
  static TextStyle titleText = TextStyle(fontSize: ScreenUtil().setSp(75), fontWeight: FontWeight.bold);
  static TextStyle itemTitleText = TextStyle(fontSize: ScreenUtil().setSp(50));
}

import 'package:flutter/material.dart';

// usage: Container(color: AppColors.container)
class AppColors {
  static Color themeBlue = const Color(0xff3b82f6);
  static Color themeGrey = const Color(0xffE5E4E2);
  static Color darkGrey = const Color(0xffd9d9d9);
  static Color darkDarkGrey = const Color(0xff888888);
  static Color lightGrey = const Color(0xffececec);
  static Color themeBlack = const Color(0xff000000);

}
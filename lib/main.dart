import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_smart_dialog/flutter_smart_dialog.dart';
import 'package:my_app/weather_model.dart';
import 'package:my_app/weather_page.dart';
import 'package:provider/provider.dart';

import 'Res/colors.dart';
import 'Res/styles.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: const Size(1080, 2220),
      minTextAdapt: true,
      splitScreenMode: true,
      builder: (context, child) {
        return MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Weather App',
          // You can use the library anywhere in the app even in theme

          theme: ThemeData(
            // This is the theme of your application.
            //
            // Try running your application with "flutter run". You'll see the
            // application has a blue toolbar. Then, without quitting the app, try
            // changing the primarySwatch below to Colors.green and then invoke
            // "hot reload" (press "r" in the console where you ran "flutter run",
            // or simply save your changes to "hot reload" in a Flutter IDE).
            // Notice that the counter didn't reset back to zero; the application
            // is not restarted.
            primarySwatch: Colors.blue,
            appBarTheme: AppBarTheme(
                toolbarHeight: ScreenUtil().setHeight(140),
                color: AppColors.themeBlue),
            splashColor: Colors.transparent,
            // Define the default `TextTheme`. Use this to specify the default
            // text styling for headlines, titles, bodies of text, and more.
            textTheme: TextTheme(
              titleLarge: Styles.textLargeBold,
              titleMedium: Styles.textMid,
              titleSmall: Styles.textSmall,
            ),
            inputDecorationTheme: InputDecorationTheme(
              contentPadding: EdgeInsets.all(ScreenUtil().setWidth(30)),
              border: OutlineInputBorder(),
              labelStyle: TextStyle(fontSize: ScreenUtil().setSp(60)),
              hintStyle: TextStyle(
                fontSize: ScreenUtil().setSp(50),
                color: Colors.grey,
              ),
            ),
          ),
          home: child,
          // here
          navigatorObservers: [FlutterSmartDialog.observer],
          // here
          builder: FlutterSmartDialog.init(),
        );
      },
      child: ChangeNotifierProvider<WeatherModel>(
        create: (context) => WeatherModel(records: {}),
        child: const WeatherPage(),
      ),
    );
  }
}

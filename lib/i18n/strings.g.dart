/// Generated file. Do not edit.
///
/// Original: lib/i18n
/// To regenerate, run: `dart run slang`
///
/// Locales: 1
/// Strings: 265
///
/// Built on 2023-07-30 at 05:55 UTC

// coverage:ignore-file
// ignore_for_file: type=lint

import 'package:flutter/widgets.dart';
import 'package:slang/builder/model/node.dart';
import 'package:slang_flutter/slang_flutter.dart';
export 'package:slang_flutter/slang_flutter.dart';

const AppLocale _baseLocale = AppLocale.en;

/// Supported locales, see extension methods below.
///
/// Usage:
/// - LocaleSettings.setLocale(AppLocale.en) // set locale
/// - Locale locale = AppLocale.en.flutterLocale // get flutter locale from enum
/// - if (LocaleSettings.currentLocale == AppLocale.en) // locale check
enum AppLocale with BaseAppLocale<AppLocale, _StringsEn> {
	en(languageCode: 'en', build: _StringsEn.build);

	const AppLocale({required this.languageCode, this.scriptCode, this.countryCode, required this.build}); // ignore: unused_element

	@override final String languageCode;
	@override final String? scriptCode;
	@override final String? countryCode;
	@override final TranslationBuilder<AppLocale, _StringsEn> build;

	/// Gets current instance managed by [LocaleSettings].
	_StringsEn get translations => LocaleSettings.instance.translationMap[this]!;
}

/// Method A: Simple
///
/// No rebuild after locale change.
/// Translation happens during initialization of the widget (call of t).
/// Configurable via 'translate_var'.
///
/// Usage:
/// String a = t.someKey.anotherKey;
/// String b = t['someKey.anotherKey']; // Only for edge cases!
_StringsEn get t => LocaleSettings.instance.currentTranslations;

/// Method B: Advanced
///
/// All widgets using this method will trigger a rebuild when locale changes.
/// Use this if you have e.g. a settings page where the user can select the locale during runtime.
///
/// Step 1:
/// wrap your App with
/// TranslationProvider(
/// 	child: MyApp()
/// );
///
/// Step 2:
/// final t = Translations.of(context); // Get t variable.
/// String a = t.someKey.anotherKey; // Use t variable.
/// String b = t['someKey.anotherKey']; // Only for edge cases!
class Translations {
	Translations._(); // no constructor

	static _StringsEn of(BuildContext context) => InheritedLocaleData.of<AppLocale, _StringsEn>(context).translations;
}

/// The provider for method B
class TranslationProvider extends BaseTranslationProvider<AppLocale, _StringsEn> {
	TranslationProvider({required super.child}) : super(settings: LocaleSettings.instance);

	static InheritedLocaleData<AppLocale, _StringsEn> of(BuildContext context) => InheritedLocaleData.of<AppLocale, _StringsEn>(context);
}

/// Method B shorthand via [BuildContext] extension method.
/// Configurable via 'translate_var'.
///
/// Usage (e.g. in a widget's build method):
/// context.t.someKey.anotherKey
extension BuildContextTranslationsExtension on BuildContext {
	_StringsEn get t => TranslationProvider.of(this).translations;
}

/// Manages all translation instances and the current locale
class LocaleSettings extends BaseFlutterLocaleSettings<AppLocale, _StringsEn> {
	LocaleSettings._() : super(utils: AppLocaleUtils.instance);

	static final instance = LocaleSettings._();

	// static aliases (checkout base methods for documentation)
	static AppLocale get currentLocale => instance.currentLocale;
	static Stream<AppLocale> getLocaleStream() => instance.getLocaleStream();
	static AppLocale setLocale(AppLocale locale, {bool? listenToDeviceLocale = false}) => instance.setLocale(locale, listenToDeviceLocale: listenToDeviceLocale);
	static AppLocale setLocaleRaw(String rawLocale, {bool? listenToDeviceLocale = false}) => instance.setLocaleRaw(rawLocale, listenToDeviceLocale: listenToDeviceLocale);
	static AppLocale useDeviceLocale() => instance.useDeviceLocale();
	@Deprecated('Use [AppLocaleUtils.supportedLocales]') static List<Locale> get supportedLocales => instance.supportedLocales;
	@Deprecated('Use [AppLocaleUtils.supportedLocalesRaw]') static List<String> get supportedLocalesRaw => instance.supportedLocalesRaw;
	static void setPluralResolver({String? language, AppLocale? locale, PluralResolver? cardinalResolver, PluralResolver? ordinalResolver}) => instance.setPluralResolver(
		language: language,
		locale: locale,
		cardinalResolver: cardinalResolver,
		ordinalResolver: ordinalResolver,
	);
}

/// Provides utility functions without any side effects.
class AppLocaleUtils extends BaseAppLocaleUtils<AppLocale, _StringsEn> {
	AppLocaleUtils._() : super(baseLocale: _baseLocale, locales: AppLocale.values);

	static final instance = AppLocaleUtils._();

	// static aliases (checkout base methods for documentation)
	static AppLocale parse(String rawLocale) => instance.parse(rawLocale);
	static AppLocale parseLocaleParts({required String languageCode, String? scriptCode, String? countryCode}) => instance.parseLocaleParts(languageCode: languageCode, scriptCode: scriptCode, countryCode: countryCode);
	static AppLocale findDeviceLocale() => instance.findDeviceLocale();
	static List<Locale> get supportedLocales => instance.supportedLocales;
	static List<String> get supportedLocalesRaw => instance.supportedLocalesRaw;
}

// translations

// Path: <root>
class _StringsEn implements BaseTranslations<AppLocale, _StringsEn> {

	/// You can call this constructor and build your own translation instance of this locale.
	/// Constructing via the enum [AppLocale.build] is preferred.
	_StringsEn.build({Map<String, Node>? overrides, PluralResolver? cardinalResolver, PluralResolver? ordinalResolver})
		: assert(overrides == null, 'Set "translation_overrides: true" in order to enable this feature.'),
		  $meta = TranslationMetadata(
		    locale: AppLocale.en,
		    overrides: overrides ?? {},
		    cardinalResolver: cardinalResolver,
		    ordinalResolver: ordinalResolver,
		  ) {
		$meta.setFlatMapFunction(_flatMapFunction);
	}

	/// Metadata for the translations of <en>.
	@override final TranslationMetadata<AppLocale, _StringsEn> $meta;

	/// Access flat map
	dynamic operator[](String key) => $meta.getTranslation(key);

	late final _StringsEn _root = this; // ignore: unused_field

	// Translations
	String get _comment => 'Battery Test Result dialog///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////';
	String get BTW100 => 'BTW100';
	String get REGISTER => 'REGISTER';
	String get Register_your_account => 'Register your account';
	String get User_Email => 'User Email';
	String get Password => 'Password';
	String get Confirm_Password => 'Confirm Password';
	String get CREATE => 'CREATE';
	String get JOIN => 'JOIN';
	String get registration_password_correction_1 => 'User password should be at least 8 characters.';
	String get registration_password_correction_2 => 'User password should at least contains 1 numeric character.';
	String get registration_password_correction_3 => 'User password should at least contains 1 uppercase character.';
	String get registration_password_correction_4 => 'User password should at least contains 1 lowercase character.';
	String get registration_password_correction_5 => 'User password should at least contains 1 special character.';
	String get registration_password_correction_6 => 'User password should not contain your email.';
	String get plz_confirm_password => 'Please confirm your password.';
	String get password_not_match => 'Passwords do not match.';
	String get registration_organization_join_register => 'You must create or join an organization for registration.';
	String get registration_organization_name_used => 'This organization name has been used. Please choose another one.';
	String get registration_user_id_used => 'This user ID has been used. Please choose another one.';
	String get registration_organization_not_exist => 'The organization does not exist.';
	String get registration_organization_format_error => 'Organization name format incorrect.';
	String get Organization_Name => 'Organization Name';
	String get User_Password => 'User Password';
	String get LOGIN => 'LOGIN';
	String get valid_email => 'Enter valid email id as abc@gmail.com';
	String get secure_password => 'Enter secure password';
	String get WARNING => 'WARNING';
	String get NOTICE => 'NOTICE';
	String get email_format_error => 'The user email\'s format is incorrect.';
	String get email_empty => 'User email cannot be empty.';
	String get password_empty => 'User password cannot be empty.';
	String get OK => 'OK';
	String get CANCEL => 'CANCEL';
	String get No => 'No';
	String get Update => 'Update';
	String get internet_connection_error => 'Please check your internet connection.';
	String get user_not_exist => 'User does not exist.';
	String get user_email_incorrect => 'Incorrect user email. Please try again.';
	String get password_not_exist => 'Password does not exist.';
	String get password_incorrect => 'Incorrect password. Please try again';
	String get password_expired => 'Your password has expired and must be changed. Please change it on the DHC IoT website.';
	String get account_not_verify => 'Your account has not been verified yet. Please click the link in the verification email.';
	String get something_went_wrong => 'Something went wrong.';
	String get server_error => 'The server or account is current under unknown problem. Please contact DHC.';
	String get join_request_decline => 'Your organization join request has been declined. Go to DHC IoT webpage to make a new request.';
	String get join_request_waiting_approve => 'This account is currently under one of these conditions:\n1. The admin has not approve the join request yet.\n2. This account has not been assigned a role by the admin.\n3. This account does not belong to any organization.';
	String get rename_device => 'Rename Device';
	String get rename_device_succeeded => 'Successfully rename the device.';
	String get delete_device => 'Delete Device';
	String get device_info => 'Device Info';
	String get firmware_version => 'Firmware Version: ';
	String get msg_parameter_error => 'Sorry, the device might not exist or the job type is incorrect.';
	String get msg_token_error => 'Sorry. The token is unauthorized, missing or has expired. Please sign in again.';
	String get rename_device_title => 'Please name your device';
	String get remember_me => 'Remember Me';
	String get device_name => 'Device name:';
	String get Save => 'Save';
	String get enter_new_name => 'Enter new name below.';
	String get rename_device_empty => 'Device name cannot be empty.';
	String get WiFi_Management => 'Wi-Fi Management';
	String get Licenses => 'Licenses';
	String get Firmware_update_available => 'Firmware update available.';
	String get firmware_up_to_date => 'Firmware is up to date.';
	String get update_in_progress => 'Update in progress.';
	String get change_group => 'Change Group';
	String get select_new_group => 'Select a new group for this device';
	String get group_path => 'Group Path:';
	String get device_info_place_group => 'This device has been successfully placed to\n';
	String get device_info_same_group => 'This device has already been assigned to ';
	String get schedule_job => 'Schedule';
	String get SUN => 'SUN';
	String get MON => 'MON';
	String get TUE => 'TUE';
	String get WED => 'WED';
	String get THU => 'THU';
	String get FRI => 'FRI';
	String get SAT => 'SAT';
	String get Sunday => 'Sunday';
	String get Monday => 'Monday';
	String get Tuesday => 'Tuesday';
	String get Wednesday => 'Wednesday';
	String get Thursday => 'Thursday';
	String get Friday => 'Friday';
	String get Saturday => 'Saturday';
	String get Everyday => 'Everyday';
	String get add_scheduled_jobs => 'Add Scheduled Jobs';
	String get job_type => 'Job Type';
	String get select_day_of_week => 'Select Day of the Week';
	String get select_time => 'Select Time (HH:MM)';
	String get add_job_description => 'I understand that battery condition will be judged under the premise that they are fully charged.';
	String get Add => 'Add';
	String get Cancel => 'Cancel';
	String get ask_log_out => 'Do you want to log out?';
	String get Events => 'Events';
	String get Devices => 'Devices';
	String get Group_Selector => 'Group Selector';
	String get Rename_Group => 'Rename Group';
	String get Group_Name => 'Group Name';
	String has_device_or_group({required Object group_name}) => '"${group_name}" still contains devices and other groups, please relocate or remove them first, then try again.';
	String confirm_delete({required Object group_name}) => '"${group_name}" will be deleted.\nContinue?';
	String get Yes => 'Yes';
	String get Back => 'Back';
	String Path({required Object group_path}) => 'Path: ${group_path}';
	String get Create_Group => 'Create Group';
	String get list_group_error => 'The group does not exist in iot device management.';
	String get Device_List => 'Device List';
	String get Group => 'Group';
	String get Battery_Testing_Parameter => 'Battery Testing Parameter';
	String get PACK => 'PACK';
	String get BAT1 => 'BAT. 1';
	String get BAT2 => 'BAT. 2';
	String get Mandatory_field => 'Mandatory field';
	String get battery_application => 'Battery Application';
	String get system_voltage => 'System Voltage';
	String get battery_type => 'Battery Type';
	String get rating => 'Rating';
	String get set_capacity => 'Set Capacity';
	String get SLI => 'SLI';
	String get Deep_Cycle => 'Deep Cycle';
	String get FLOODED => 'FLOODED';
	String get AGM => 'AGM';
	String get AGMF => 'AGM FLAT PLATE';
	String get AGMS => 'AGM SPIRAL';
	String get VRLA_GEL => 'VRLA/GEL';
	String get EFB => 'EFB';
	String get MF => 'MF';
	String get CCA_SAE => 'CCA/SAE';
	String get EN => 'EN';
	String get EN2 => 'EN2';
	String get IEC => 'IEC';
	String get JIS => 'JIS';
	String get DIN => 'DIN';
	String get CA_MCA => 'CA/MCA';
	String get initial_Ir => 'Initial IR';
	String get normal_threshold => 'Normal Threshold';
	String get caution_threshold => 'Caution Threshold';
	String get replace_threshold => 'Replace Threshold';
	String get Test => 'Test';
	String get capacity_validation => 'Capacity must matches the following conditions:\n\n1. Must be an integer.\n2. Must be a multiple of 5.\n3. Must between ';
	String get Saved => 'Saved';
	String get ir_test_failed => 'Initial IR Test failed.\nPlease try again.\n';
	String get check_validation => 'Please ensure the following conditions:\n1. The normal threshold is between 100-150.\n2. The caution threshold is between 150-300.\n3. The normal threshold is lower than the caution threshold.';
	String get Vehicle_Information => 'Vehicle Information';
	String get Note => 'Note';
	String get Battery_Installation_Date => 'Battery Installation Date';
	String get Battery_Information => 'Battery Information';
	String get Battery_Capacity_Ah => 'Battery Capacity (Ah)';
	String get Select_Date => 'Select Date';
	String get update_both_inir => 'The initial IR of both battery 1 and 2 will be updated to their current IR, proceed?';
	String get fields_cannot_empty => 'The mandatory fields cannot be empty.';
	String get Device => 'Device';
	String get Dashboard => 'Dashboard';
	String get Job => 'Job';
	String get Live_Voltage => 'Live Voltage';
	String get Live_Temperature => 'Live Temperature';
	String get Device_Settings => 'Device Settings';
	String get Action => 'Action';
	String get Show_more => 'Show more';
	String get Last_Updated => 'Last Updated';
	String get Last_Tested => 'Last Tested';
	String get device_not_exist_error => 'The device does not exist in iot device management.';
	String get Queued_Jobs => 'Queued Jobs';
	String get Firmware_Update => 'Firmware Update';
	String get Battery_Test => 'Battery Test';
	String get Set_Threshold => 'Set Threshold';
	String get Battery_Test_for_Analytics => 'Battery Test for Analytics';
	String get Initial_IR_Test => 'Initial IR Test';
	String get Event_Log => 'Event Log';
	String get nl_Battery_Test => 'Battery\nTest';
	String get nl_Set_Threshold => 'Set\nThreshold';
	String get nl_Edit_Schedule => 'Edit\nSchedule';
	String get nl_Event_Log => 'Event\nLog';
	String get Is_battery_charge => 'Is battery charge?';
	String get Please_enter_a_number_in_the_fields => 'Invalid value, the fields should not be empty and only accept Arabic numerals.';
	String get Successfully_created_set_threshold_job => 'Successfully created set threshold job.';
	String get Successfully_created_battery_test_job => 'Successfully created battery test job.';
	String get Nothing_in_progress => 'Nothing in progress';
	String get In_Progress => 'In Progress';
	String get Set_Voltage_Threshold_Failed => 'Set Voltage Threshold Failed';
	String get Set_Voltage_Threshold_Success => 'Set Voltage Threshold Success';
	String get Voltage_High => 'Voltage High';
	String get Voltage_Normal => 'Voltage Normal';
	String get Voltage_Low => 'Voltage Low';
	String get Set_Temperature_Threshold_Failed => 'Set Temperature Threshold Failed';
	String get Set_Temperature_Threshold_Success => 'Set Temperature Threshold Success';
	String get Temperature_High => 'Temperature High';
	String get Temperature_Normal => 'Temperature Normal';
	String get Temperature_Low => 'Temperature Low';
	String get Firmware_Update_Failed => 'Firmware Update Failed';
	String get Firmware_Update_Success => 'Firmware Update Success';
	String get Firmware_Update_Available => 'Firmware Update Available';
	String get Battery_Test_Failed => 'Battery Test Failed';
	String get Good_Pack => 'Good Pack';
	String get Check_Pack => 'Check Pack';
	String get Bad_connection => 'Bad connection';
	String get Good_Pass => 'Good & Pass';
	String get Good_Recharge => 'Good & Recharge';
	String get Bad_Replace => 'Bad & Replace';
	String get Caution => 'Caution';
	String get Recharge_Retest => 'Recharge & Retest';
	String get Bad_Cell_Replace => 'Bad Cell Replace';
	String get Battery_Test_for_Analytics_Failed => 'Battery Test for Analytics Failed';
	String get Offline => 'Offline';
	String get Online => 'Online';
	String get ERROR_TYPE => 'ERROR TYPE';
	String get ERROR_DETAIL => 'ERROR DETAIL';
	String get Battery => 'Battery';
	String get Result => 'Result';
	String get add_new_device => 'Add A New Device';
	String get add_new_device_sub => 'This process will add a new DHC IoT device to your organization.\n';
	String get add_new_device_preparation_1 => 'Before you start:\n 1. Connect DHC IoT device to a battery or batteries.\n';
	String get add_new_device_preparation_2 => ' 2. Make sure the device is in pairing mode. Status light on the device will alternate between green and red when in paring mode.\n';
	String get add_new_device_preparation_3 => ' 3. Make sure the Wi-Fi hotspot you want to connect to is in range.';
	String get add_new_device_warning => 'Warning:\nThis process cannot be aborted once started.';
	String get add_new_device_continue => 'Press "Pair" to continue.';
	String get Pair => 'Pair';
	String get connect_to_device => 'Please connect directly to DHC IoT device you want to pair.';
	String get connect_to_device_preparation_1 => ' 1. Go to the Wi-Fi control panel of your phone or tablet.\n';
	String get connect_to_device_preparation_2 => ' 2. Connect directly to the DHC IoT device you want to pair.\n';
	String get connect_to_device_preparation_3 => ' 3. Come back to this app to continue the pairing process.\n';
	String get found_device => 'Successfully connect to ';
	String get not_connect_device => 'Please connect to DHC IoT device.';
	String get permission_grated => 'Please grant the location permission in your app settings.';
	String get set_time_zone => 'Set device time zone.';
	String get select_wifi_for_device => 'Select a Wi-Fi network for the device.';
	String get available_wifi => 'Available Wi-Fi Networks';
	String get Search => 'Search';
	String get connect_wifi_dialog_title => 'Enter Password';
	String get connect_wifi_dialog_subtitle => 'Enter password for ';
	String get Connect => 'Connect';
	String get tcp_build_error => 'Please ensure you are connected to DHC IoT device.';
	String get config_not_empty => 'This device has been configured with other Wi-Fi before.\nPlease access Wi-Fi management to modify its configuration.';
	String get connect_fail => 'Fail to connect to ';
	String get please_try_again => 'Please try again.';
	String get tcp_communication_error => 'The password might be incorrect or the connection between your mobile and the DHC IoT device is aborted.\nPlease try again.';
	String get connect_to_router_wifi => 'Please connect to a Wi-Fi network with internet.';
	String get connect_to_router_content => '1. Go to the Wi-Fi control panel of your phone or tablet.\n2. Connect to a Wi-Fi network with internet.\n3. Come back to this app to continue the pairing process.';
	String get select_group_subtitle => 'Select a group to put the new device.';
	String get device_paired => 'Device Paired';
	String get device_paired_content => 'The device has been successfully paired.\nPath: ';
	String get device_paired_before => 'This device has been paired.\nTo manage device Wi-Fi network, go to\nDevicePage/ Device Settings / Wi-Fi Management.';
	String get wifi_management_subtitle => 'You can edit the known Wi-Fi networks from here.';
	String get wifi_management_content => 'To begin, press and hold the command button for 3 to 5 seconds. The button should be released once you see the status LED is alternating between red and green.';
	String get Next => 'Next';
	String get known_wifi => 'Known Wi-Fi Networks';
	String get maximum_config => 'A device can only be paired with 3 Wi-Fi configs.\nPlease delete some configs ahead to proceed.';
	String get exit_wifi_management => 'Exit Wi-Fi Management';
	String get Threshold_Type => 'Threshold Type';
	String get Voltage => 'Voltage';
	String get Temperature => 'Temperature';
	String get High_Limit => 'High Limit';
	String get Low_Limit => 'Low Limit';
	String get Close => 'Close';
	String get Set_Threshold_Result => 'Set Threshold Result';
	String get Voltage_Threshold => 'Voltage Threshold';
	String get Temperature_Threshold => 'Temperature Threshold';
	String get Original_Threshold => 'Original Threshold';
	String get New_Threshold => 'New Threshold';
	String get High => 'High';
	String get Low => 'Low';
	String get Firmware_Update_Result => 'Firmware Update Result';
	String get Original_Version => 'Original Version';
	String get New_Version => 'New Version';
	String get Battery_Test_Result => 'Battery Test Result';
}

/// Flat map(s) containing all translations.
/// Only for edge cases! For simple maps, use the map function of this library.

extension on _StringsEn {
	dynamic _flatMapFunction(String path) {
		switch (path) {
			case '_comment': return 'Battery Test Result dialog///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////';
			case 'BTW100': return 'BTW100';
			case 'REGISTER': return 'REGISTER';
			case 'Register_your_account': return 'Register your account';
			case 'User_Email': return 'User Email';
			case 'Password': return 'Password';
			case 'Confirm_Password': return 'Confirm Password';
			case 'CREATE': return 'CREATE';
			case 'JOIN': return 'JOIN';
			case 'registration_password_correction_1': return 'User password should be at least 8 characters.';
			case 'registration_password_correction_2': return 'User password should at least contains 1 numeric character.';
			case 'registration_password_correction_3': return 'User password should at least contains 1 uppercase character.';
			case 'registration_password_correction_4': return 'User password should at least contains 1 lowercase character.';
			case 'registration_password_correction_5': return 'User password should at least contains 1 special character.';
			case 'registration_password_correction_6': return 'User password should not contain your email.';
			case 'plz_confirm_password': return 'Please confirm your password.';
			case 'password_not_match': return 'Passwords do not match.';
			case 'registration_organization_join_register': return 'You must create or join an organization for registration.';
			case 'registration_organization_name_used': return 'This organization name has been used. Please choose another one.';
			case 'registration_user_id_used': return 'This user ID has been used. Please choose another one.';
			case 'registration_organization_not_exist': return 'The organization does not exist.';
			case 'registration_organization_format_error': return 'Organization name format incorrect.';
			case 'Organization_Name': return 'Organization Name';
			case 'User_Password': return 'User Password';
			case 'LOGIN': return 'LOGIN';
			case 'valid_email': return 'Enter valid email id as abc@gmail.com';
			case 'secure_password': return 'Enter secure password';
			case 'WARNING': return 'WARNING';
			case 'NOTICE': return 'NOTICE';
			case 'email_format_error': return 'The user email\'s format is incorrect.';
			case 'email_empty': return 'User email cannot be empty.';
			case 'password_empty': return 'User password cannot be empty.';
			case 'OK': return 'OK';
			case 'CANCEL': return 'CANCEL';
			case 'No': return 'No';
			case 'Update': return 'Update';
			case 'internet_connection_error': return 'Please check your internet connection.';
			case 'user_not_exist': return 'User does not exist.';
			case 'user_email_incorrect': return 'Incorrect user email. Please try again.';
			case 'password_not_exist': return 'Password does not exist.';
			case 'password_incorrect': return 'Incorrect password. Please try again';
			case 'password_expired': return 'Your password has expired and must be changed. Please change it on the DHC IoT website.';
			case 'account_not_verify': return 'Your account has not been verified yet. Please click the link in the verification email.';
			case 'something_went_wrong': return 'Something went wrong.';
			case 'server_error': return 'The server or account is current under unknown problem. Please contact DHC.';
			case 'join_request_decline': return 'Your organization join request has been declined. Go to DHC IoT webpage to make a new request.';
			case 'join_request_waiting_approve': return 'This account is currently under one of these conditions:\n1. The admin has not approve the join request yet.\n2. This account has not been assigned a role by the admin.\n3. This account does not belong to any organization.';
			case 'rename_device': return 'Rename Device';
			case 'rename_device_succeeded': return 'Successfully rename the device.';
			case 'delete_device': return 'Delete Device';
			case 'device_info': return 'Device Info';
			case 'firmware_version': return 'Firmware Version: ';
			case 'msg_parameter_error': return 'Sorry, the device might not exist or the job type is incorrect.';
			case 'msg_token_error': return 'Sorry. The token is unauthorized, missing or has expired. Please sign in again.';
			case 'rename_device_title': return 'Please name your device';
			case 'remember_me': return 'Remember Me';
			case 'device_name': return 'Device name:';
			case 'Save': return 'Save';
			case 'enter_new_name': return 'Enter new name below.';
			case 'rename_device_empty': return 'Device name cannot be empty.';
			case 'WiFi_Management': return 'Wi-Fi Management';
			case 'Licenses': return 'Licenses';
			case 'Firmware_update_available': return 'Firmware update available.';
			case 'firmware_up_to_date': return 'Firmware is up to date.';
			case 'update_in_progress': return 'Update in progress.';
			case 'change_group': return 'Change Group';
			case 'select_new_group': return 'Select a new group for this device';
			case 'group_path': return 'Group Path:';
			case 'device_info_place_group': return 'This device has been successfully placed to\n';
			case 'device_info_same_group': return 'This device has already been assigned to ';
			case 'schedule_job': return 'Schedule';
			case 'SUN': return 'SUN';
			case 'MON': return 'MON';
			case 'TUE': return 'TUE';
			case 'WED': return 'WED';
			case 'THU': return 'THU';
			case 'FRI': return 'FRI';
			case 'SAT': return 'SAT';
			case 'Sunday': return 'Sunday';
			case 'Monday': return 'Monday';
			case 'Tuesday': return 'Tuesday';
			case 'Wednesday': return 'Wednesday';
			case 'Thursday': return 'Thursday';
			case 'Friday': return 'Friday';
			case 'Saturday': return 'Saturday';
			case 'Everyday': return 'Everyday';
			case 'add_scheduled_jobs': return 'Add Scheduled Jobs';
			case 'job_type': return 'Job Type';
			case 'select_day_of_week': return 'Select Day of the Week';
			case 'select_time': return 'Select Time (HH:MM)';
			case 'add_job_description': return 'I understand that battery condition will be judged under the premise that they are fully charged.';
			case 'Add': return 'Add';
			case 'Cancel': return 'Cancel';
			case 'ask_log_out': return 'Do you want to log out?';
			case 'Events': return 'Events';
			case 'Devices': return 'Devices';
			case 'Group_Selector': return 'Group Selector';
			case 'Rename_Group': return 'Rename Group';
			case 'Group_Name': return 'Group Name';
			case 'has_device_or_group': return ({required Object group_name}) => '"${group_name}" still contains devices and other groups, please relocate or remove them first, then try again.';
			case 'confirm_delete': return ({required Object group_name}) => '"${group_name}" will be deleted.\nContinue?';
			case 'Yes': return 'Yes';
			case 'Back': return 'Back';
			case 'Path': return ({required Object group_path}) => 'Path: ${group_path}';
			case 'Create_Group': return 'Create Group';
			case 'list_group_error': return 'The group does not exist in iot device management.';
			case 'Device_List': return 'Device List';
			case 'Group': return 'Group';
			case 'Battery_Testing_Parameter': return 'Battery Testing Parameter';
			case 'PACK': return 'PACK';
			case 'BAT1': return 'BAT. 1';
			case 'BAT2': return 'BAT. 2';
			case 'Mandatory_field': return 'Mandatory field';
			case 'battery_application': return 'Battery Application';
			case 'system_voltage': return 'System Voltage';
			case 'battery_type': return 'Battery Type';
			case 'rating': return 'Rating';
			case 'set_capacity': return 'Set Capacity';
			case 'SLI': return 'SLI';
			case 'Deep_Cycle': return 'Deep Cycle';
			case 'FLOODED': return 'FLOODED';
			case 'AGM': return 'AGM';
			case 'AGMF': return 'AGM FLAT PLATE';
			case 'AGMS': return 'AGM SPIRAL';
			case 'VRLA_GEL': return 'VRLA/GEL';
			case 'EFB': return 'EFB';
			case 'MF': return 'MF';
			case 'CCA_SAE': return 'CCA/SAE';
			case 'EN': return 'EN';
			case 'EN2': return 'EN2';
			case 'IEC': return 'IEC';
			case 'JIS': return 'JIS';
			case 'DIN': return 'DIN';
			case 'CA_MCA': return 'CA/MCA';
			case 'initial_Ir': return 'Initial IR';
			case 'normal_threshold': return 'Normal Threshold';
			case 'caution_threshold': return 'Caution Threshold';
			case 'replace_threshold': return 'Replace Threshold';
			case 'Test': return 'Test';
			case 'capacity_validation': return 'Capacity must matches the following conditions:\n\n1. Must be an integer.\n2. Must be a multiple of 5.\n3. Must between ';
			case 'Saved': return 'Saved';
			case 'ir_test_failed': return 'Initial IR Test failed.\nPlease try again.\n';
			case 'check_validation': return 'Please ensure the following conditions:\n1. The normal threshold is between 100-150.\n2. The caution threshold is between 150-300.\n3. The normal threshold is lower than the caution threshold.';
			case 'Vehicle_Information': return 'Vehicle Information';
			case 'Note': return 'Note';
			case 'Battery_Installation_Date': return 'Battery Installation Date';
			case 'Battery_Information': return 'Battery Information';
			case 'Battery_Capacity_Ah': return 'Battery Capacity (Ah)';
			case 'Select_Date': return 'Select Date';
			case 'update_both_inir': return 'The initial IR of both battery 1 and 2 will be updated to their current IR, proceed?';
			case 'fields_cannot_empty': return 'The mandatory fields cannot be empty.';
			case 'Device': return 'Device';
			case 'Dashboard': return 'Dashboard';
			case 'Job': return 'Job';
			case 'Live_Voltage': return 'Live Voltage';
			case 'Live_Temperature': return 'Live Temperature';
			case 'Device_Settings': return 'Device Settings';
			case 'Action': return 'Action';
			case 'Show_more': return 'Show more';
			case 'Last_Updated': return 'Last Updated';
			case 'Last_Tested': return 'Last Tested';
			case 'device_not_exist_error': return 'The device does not exist in iot device management.';
			case 'Queued_Jobs': return 'Queued Jobs';
			case 'Firmware_Update': return 'Firmware Update';
			case 'Battery_Test': return 'Battery Test';
			case 'Set_Threshold': return 'Set Threshold';
			case 'Battery_Test_for_Analytics': return 'Battery Test for Analytics';
			case 'Initial_IR_Test': return 'Initial IR Test';
			case 'Event_Log': return 'Event Log';
			case 'nl_Battery_Test': return 'Battery\nTest';
			case 'nl_Set_Threshold': return 'Set\nThreshold';
			case 'nl_Edit_Schedule': return 'Edit\nSchedule';
			case 'nl_Event_Log': return 'Event\nLog';
			case 'Is_battery_charge': return 'Is battery charge?';
			case 'Please_enter_a_number_in_the_fields': return 'Invalid value, the fields should not be empty and only accept Arabic numerals.';
			case 'Successfully_created_set_threshold_job': return 'Successfully created set threshold job.';
			case 'Successfully_created_battery_test_job': return 'Successfully created battery test job.';
			case 'Nothing_in_progress': return 'Nothing in progress';
			case 'In_Progress': return 'In Progress';
			case 'Set_Voltage_Threshold_Failed': return 'Set Voltage Threshold Failed';
			case 'Set_Voltage_Threshold_Success': return 'Set Voltage Threshold Success';
			case 'Voltage_High': return 'Voltage High';
			case 'Voltage_Normal': return 'Voltage Normal';
			case 'Voltage_Low': return 'Voltage Low';
			case 'Set_Temperature_Threshold_Failed': return 'Set Temperature Threshold Failed';
			case 'Set_Temperature_Threshold_Success': return 'Set Temperature Threshold Success';
			case 'Temperature_High': return 'Temperature High';
			case 'Temperature_Normal': return 'Temperature Normal';
			case 'Temperature_Low': return 'Temperature Low';
			case 'Firmware_Update_Failed': return 'Firmware Update Failed';
			case 'Firmware_Update_Success': return 'Firmware Update Success';
			case 'Firmware_Update_Available': return 'Firmware Update Available';
			case 'Battery_Test_Failed': return 'Battery Test Failed';
			case 'Good_Pack': return 'Good Pack';
			case 'Check_Pack': return 'Check Pack';
			case 'Bad_connection': return 'Bad connection';
			case 'Good_Pass': return 'Good & Pass';
			case 'Good_Recharge': return 'Good & Recharge';
			case 'Bad_Replace': return 'Bad & Replace';
			case 'Caution': return 'Caution';
			case 'Recharge_Retest': return 'Recharge & Retest';
			case 'Bad_Cell_Replace': return 'Bad Cell Replace';
			case 'Battery_Test_for_Analytics_Failed': return 'Battery Test for Analytics Failed';
			case 'Offline': return 'Offline';
			case 'Online': return 'Online';
			case 'ERROR_TYPE': return 'ERROR TYPE';
			case 'ERROR_DETAIL': return 'ERROR DETAIL';
			case 'Battery': return 'Battery';
			case 'Result': return 'Result';
			case 'add_new_device': return 'Add A New Device';
			case 'add_new_device_sub': return 'This process will add a new DHC IoT device to your organization.\n';
			case 'add_new_device_preparation_1': return 'Before you start:\n 1. Connect DHC IoT device to a battery or batteries.\n';
			case 'add_new_device_preparation_2': return ' 2. Make sure the device is in pairing mode. Status light on the device will alternate between green and red when in paring mode.\n';
			case 'add_new_device_preparation_3': return ' 3. Make sure the Wi-Fi hotspot you want to connect to is in range.';
			case 'add_new_device_warning': return 'Warning:\nThis process cannot be aborted once started.';
			case 'add_new_device_continue': return 'Press "Pair" to continue.';
			case 'Pair': return 'Pair';
			case 'connect_to_device': return 'Please connect directly to DHC IoT device you want to pair.';
			case 'connect_to_device_preparation_1': return ' 1. Go to the Wi-Fi control panel of your phone or tablet.\n';
			case 'connect_to_device_preparation_2': return ' 2. Connect directly to the DHC IoT device you want to pair.\n';
			case 'connect_to_device_preparation_3': return ' 3. Come back to this app to continue the pairing process.\n';
			case 'found_device': return 'Successfully connect to ';
			case 'not_connect_device': return 'Please connect to DHC IoT device.';
			case 'permission_grated': return 'Please grant the location permission in your app settings.';
			case 'set_time_zone': return 'Set device time zone.';
			case 'select_wifi_for_device': return 'Select a Wi-Fi network for the device.';
			case 'available_wifi': return 'Available Wi-Fi Networks';
			case 'Search': return 'Search';
			case 'connect_wifi_dialog_title': return 'Enter Password';
			case 'connect_wifi_dialog_subtitle': return 'Enter password for ';
			case 'Connect': return 'Connect';
			case 'tcp_build_error': return 'Please ensure you are connected to DHC IoT device.';
			case 'config_not_empty': return 'This device has been configured with other Wi-Fi before.\nPlease access Wi-Fi management to modify its configuration.';
			case 'connect_fail': return 'Fail to connect to ';
			case 'please_try_again': return 'Please try again.';
			case 'tcp_communication_error': return 'The password might be incorrect or the connection between your mobile and the DHC IoT device is aborted.\nPlease try again.';
			case 'connect_to_router_wifi': return 'Please connect to a Wi-Fi network with internet.';
			case 'connect_to_router_content': return '1. Go to the Wi-Fi control panel of your phone or tablet.\n2. Connect to a Wi-Fi network with internet.\n3. Come back to this app to continue the pairing process.';
			case 'select_group_subtitle': return 'Select a group to put the new device.';
			case 'device_paired': return 'Device Paired';
			case 'device_paired_content': return 'The device has been successfully paired.\nPath: ';
			case 'device_paired_before': return 'This device has been paired.\nTo manage device Wi-Fi network, go to\nDevicePage/ Device Settings / Wi-Fi Management.';
			case 'wifi_management_subtitle': return 'You can edit the known Wi-Fi networks from here.';
			case 'wifi_management_content': return 'To begin, press and hold the command button for 3 to 5 seconds. The button should be released once you see the status LED is alternating between red and green.';
			case 'Next': return 'Next';
			case 'known_wifi': return 'Known Wi-Fi Networks';
			case 'maximum_config': return 'A device can only be paired with 3 Wi-Fi configs.\nPlease delete some configs ahead to proceed.';
			case 'exit_wifi_management': return 'Exit Wi-Fi Management';
			case 'Threshold_Type': return 'Threshold Type';
			case 'Voltage': return 'Voltage';
			case 'Temperature': return 'Temperature';
			case 'High_Limit': return 'High Limit';
			case 'Low_Limit': return 'Low Limit';
			case 'Close': return 'Close';
			case 'Set_Threshold_Result': return 'Set Threshold Result';
			case 'Voltage_Threshold': return 'Voltage Threshold';
			case 'Temperature_Threshold': return 'Temperature Threshold';
			case 'Original_Threshold': return 'Original Threshold';
			case 'New_Threshold': return 'New Threshold';
			case 'High': return 'High';
			case 'Low': return 'Low';
			case 'Firmware_Update_Result': return 'Firmware Update Result';
			case 'Original_Version': return 'Original Version';
			case 'New_Version': return 'New Version';
			case 'Battery_Test_Result': return 'Battery Test Result';
			default: return null;
		}
	}
}
